﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class Klienci
    {
        public Klienci()
        {
            Zamowienia = new HashSet<Zamowienium>();
        }

        public int IdKlienta { get; set; }
        [DisplayName("Email Klienta")]
        public string EmailKlienta { get; set; }
        [DisplayName("Adres Klienta")]
        public string AdresKlienta { get; set; }

        public virtual ICollection<Zamowienium> Zamowienia { get; set; }
    }
}
