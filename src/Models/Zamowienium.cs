﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class Zamowienium
    {
        [DisplayName("Zamówienie")]
        public int IdZamowienia { get; set; }
        [DisplayName("Klient")]
        public int IdKlienta { get; set; }
        [DisplayName("Wartość zamówienia")]
        public decimal WartoscZamowienia { get; set; }
        [DisplayName("Dzień zlożenia zamówienia")]
        public DateTime DataZlozeniaZamowienia { get; set; }
        [DisplayName("Status zamówienia")]
        public int IdStatusuZamowienia { get; set; }

        public virtual Klienci IdKlientaNavigation { get; set; }
        public virtual StatusyZamowienium IdStatusuZamowieniaNavigation { get; set; }
    }
}
