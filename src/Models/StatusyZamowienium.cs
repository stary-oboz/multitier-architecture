﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class StatusyZamowienium
    {
        public StatusyZamowienium()
        {
            Zamowienia = new HashSet<Zamowienium>();
        }

        public int IdStatusu { get; set; }
        [DisplayName("Status")]
        public string NazwaStatusu { get; set; }

        public virtual ICollection<Zamowienium> Zamowienia { get; set; }
    }
}
