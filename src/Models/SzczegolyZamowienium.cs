﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
#nullable disable

namespace SklepDotNet.Models
{
    public partial class SzczegolyZamowienium
    {
        [Key]
        public int IdSzczegolyZamowienia { get; set; }

        public int IdZamowienia { get; set; }

        public int IdProduktu { get; set; }
        [DisplayName("Ilość zamawianego produktu")]
        public int IloscZamowionegoProduktu { get; set; }
        [DisplayName("Produkt - odniesienie")]
        public virtual Produkty IdProduktuNavigation { get; set; }
        [DisplayName("Zamówienie")]
        public virtual Zamowienium IdZamowieniaNavigation { get; set; }
    }
}
