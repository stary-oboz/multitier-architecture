﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class ShopDotNetContext : DbContext
    {
        public ShopDotNetContext()
        {
        }

        public ShopDotNetContext(DbContextOptions<ShopDotNetContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Kategorie> Kategories { get; set; }
        public virtual DbSet<Klienci> Kliencis { get; set; }
        public virtual DbSet<Produkty> Produkties { get; set; }
        public virtual DbSet<StatusyZamowienium> StatusyZamowienia { get; set; }
        public virtual DbSet<SzczegolyZamowienium> SzczegolyZamowienia { get; set; }
        public virtual DbSet<Zamowienium> Zamowienia { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=LAPTOP-TA0OQUK4\\SQLDEVELOPER1;Database=ShopDotNet;Trusted_Connection=true;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Polish_CI_AS");

            modelBuilder.Entity<Kategorie>(entity =>
            {
                entity.HasKey(e => e.IdKategorii);

                entity.ToTable("Kategorie");

                entity.Property(e => e.NazwaKategorii)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<Klienci>(entity =>
            {
                entity.HasKey(e => e.IdKlienta);

                entity.ToTable("Klienci");

                entity.Property(e => e.AdresKlienta)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.EmailKlienta)
                    .IsRequired()
                    .HasColumnType("text");
            });

            

            modelBuilder.Entity<Produkty>(entity =>
            {
                entity.ToTable("Produkty");

                entity.Property(e => e.CenaProduktu).HasColumnType("money");

                entity.Property(e => e.NazwaProduktu)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.OpisProduktu)
                    .IsRequired()
                    .HasColumnType("text");

                entity.HasOne(d => d.IdKategoriaNavigation)
                    .WithMany(p => p.Produkties)
                    .HasForeignKey(d => d.IdKategoria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Produkty_Kategorie");
                           
                });

            modelBuilder.Entity<StatusyZamowienium>(entity =>
            {
                entity.HasKey(e => e.IdStatusu);

                entity.Property(e => e.NazwaStatusu)
                    .IsRequired()
                    .HasColumnType("text");
            });

            modelBuilder.Entity<SzczegolyZamowienium>(entity =>
            {
                entity.HasKey(d=>d.IdSzczegolyZamowienia);

                entity.HasOne(d => d.IdProduktuNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdProduktu)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SzczegolyZamowienia_Produkty");

                entity.HasOne(d => d.IdZamowieniaNavigation)
                    .WithMany()
                    .HasForeignKey(d => d.IdZamowienia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_SzczegolyZamowienia_Zamowienia");
            });

            modelBuilder.Entity<Zamowienium>(entity =>
            {
                entity.HasKey(e => e.IdZamowienia);

                entity.Property(e => e.DataZlozeniaZamowienia).HasColumnType("date");

                entity.Property(e => e.WartoscZamowienia).HasColumnType("money");

                entity.HasOne(d => d.IdKlientaNavigation)
                    .WithMany(p => p.Zamowienia)
                    .HasForeignKey(d => d.IdKlienta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zamowienia_Klienci");

                entity.HasOne(d => d.IdStatusuZamowieniaNavigation)
                    .WithMany(p => p.Zamowienia)
                    .HasForeignKey(d => d.IdStatusuZamowienia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zamowienia_StatusyZamowienia");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
