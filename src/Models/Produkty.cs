﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class Produkty
    {
        public int Id { get; set; }
        [DisplayName("Nazwa produktu")]
        public string NazwaProduktu { get; set; }
        [DisplayName("Opis produktu")]
        public string OpisProduktu { get; set; }
        [DisplayName("Cena produktu")]
        [Required(ErrorMessage ="Podaj cenę.")]
       // [Range(typeof(decimal),"9.01","100000.00",ErrorMessage ="Podaj prawidłową cenę")]
        //[RegularExpression(@"^[0-9]{1,6}\.[0-9]{2}$", ErrorMessage = "Podaj prawidłową cenę.")]
        public decimal CenaProduktu { get; set; }

        [DisplayName("Ilość produku w magazynie")]
        public int IloscWMagazynie { get; set; }
        [DisplayName("Kategoria produktu")]
        public int IdKategoria { get; set; }

        [DisplayName("Kategoria produktu")]
        public virtual Kategorie IdKategoriaNavigation { get; set; }
    }
}
