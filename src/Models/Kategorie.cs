﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

#nullable disable

namespace SklepDotNet.Models
{
    public partial class Kategorie
    {
        public Kategorie()
        {
            Produkties = new HashSet<Produkty>();
        }

        public int IdKategorii { get; set; }
        [DisplayName("Nazwa kategorii")]
        public string NazwaKategorii { get; set; }

        public virtual ICollection<Produkty> Produkties { get; set; }
    }
}
