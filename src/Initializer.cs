﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Infrastructure;
using SklepDotNet.Models;


namespace SklepDotNet
{
    public static class Initializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var opcje = serviceProvider.GetRequiredService<DbContextOptions<ShopDotNetContext>>();
            using (var context = new ShopDotNetContext(opcje))
            {
                // context.Database.EnsureDeleted();
                //context.Database.EnsureCreated();


                context.SaveChanges();

            }
        }
    }
}
