﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SklepDotNet.Models;

namespace SklepDotNet.Controllers
{
    public class ProduktyController : Controller
    {
        private readonly ShopDotNetContext _context;

        public ProduktyController(ShopDotNetContext context)
        {
            _context = context;
        }

        // GET: Produkty
        public async Task<IActionResult> Index()
        {
            var shopDotNetContext = _context.Produkties.Include(p => p.IdKategoriaNavigation);
            
            return View(await shopDotNetContext.ToListAsync());
        }

        // GET: Produkty/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var produkty = await _context.Produkties
                .Include(p => p.IdKategoriaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (produkty == null)
            {
                return NotFound();
            }

            return View(produkty);
        }

        // GET: Produkty/Create
        public IActionResult Create()
        {
            ViewData["IdKategoria"] = new SelectList(_context.Kategories, "IdKategorii", "NazwaKategorii");
           
            return View();
        }
        [HttpPost]
        public IActionResult Index(string SzukajProdukt)
        {
            var produkty =  _context.Produkties.FromSqlRaw(sql: "SELECT * FROM Produkty WHERE NazwaProduktu LIKE '%" + SzukajProdukt + "%'").ToList();
            return View(produkty);
        }

        // POST: Produkty/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,NazwaProduktu,OpisProduktu,CenaProduktu,IloscWMagazynie,IdKategoria")] Produkty produkty)
        {
            if (ModelState.IsValid)
            {
                _context.Add(produkty);
              
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKategoria"] = new SelectList(_context.Kategories, "IdKategorii", "NazwaKategorii", produkty.IdKategoria);
            return View(produkty);
        }

        // GET: Produkty/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var produkty = await _context.Produkties.FindAsync(id);
            if (produkty == null)
            {
                return NotFound();
            }
            ViewData["IdKategoria"] = new SelectList(_context.Kategories, "IdKategorii", "NazwaKategorii", produkty.IdKategoria);
            return View(produkty);
        }

        // POST: Produkty/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NazwaProduktu,OpisProduktu,CenaProduktu,IloscWMagazynie,IdKategoria")] Produkty produkty)
        {
            if (id != produkty.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(produkty);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProduktyExists(produkty.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKategoria"] = new SelectList(_context.Kategories, "IdKategorii", "NazwaKategorii", produkty.IdKategoria);
            return View(produkty);
        }

        // GET: Produkty/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var produkty = await _context.Produkties
                .Include(p => p.IdKategoriaNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (produkty == null)
            {
                return NotFound();
            }

            return View(produkty);
        }

        // POST: Produkty/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var produkty = await _context.Produkties.FindAsync(id);
            _context.Produkties.Remove(produkty);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProduktyExists(int id)
        {
            return _context.Produkties.Any(e => e.Id == id);
        }
    }
}
