﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SklepDotNet.Models;

namespace SklepDotNet.Controllers
{
    public class SZController : Controller
    {
        private readonly ShopDotNetContext _context;

        public SZController(ShopDotNetContext context)
        {
            _context = context;
        }

        // GET: SZ
        public async Task<IActionResult> Index()
        {
            var shopDotNetContext = _context.SzczegolyZamowienia.Include(s => s.IdProduktuNavigation).Include(s => s.IdZamowieniaNavigation);
            return View(await shopDotNetContext.ToListAsync());
        }

        // GET: SZ/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegolyZamowienium = await _context.SzczegolyZamowienia
                .Include(s => s.IdProduktuNavigation)
                .Include(s => s.IdZamowieniaNavigation)
                .FirstOrDefaultAsync(m => m.IdSzczegolyZamowienia == id);
            if (szczegolyZamowienium == null)
            {
                return NotFound();
            }

            return View(szczegolyZamowienium);
        }

        // GET: SZ/Create
        public IActionResult Create()
        {
            ViewData["IdProduktu"] = new SelectList(_context.Produkties, "Id", "NazwaProduktu");
            ViewData["IdZamowienia"] = new SelectList(_context.Zamowienia, "IdZamowienia", "IdZamowienia");
            return View();
        }

        // POST: SZ/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdSzczegolyZamowienia,IdZamowienia,IdProduktu,IloscZamowionegoProduktu")] SzczegolyZamowienium szczegolyZamowienium)
        {
            if (ModelState.IsValid)
            {
                _context.Add(szczegolyZamowienium);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdProduktu"] = new SelectList(_context.Produkties, "Id", "NazwaProduktu", szczegolyZamowienium.IdProduktu);
            ViewData["IdZamowienia"] = new SelectList(_context.Zamowienia, "IdZamowienia", "IdZamowienia", szczegolyZamowienium.IdZamowienia);
            return View(szczegolyZamowienium);
        }

        // GET: SZ/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegolyZamowienium = await _context.SzczegolyZamowienia.FindAsync(id);
            if (szczegolyZamowienium == null)
            {
                return NotFound();
            }
            ViewData["IdProduktu"] = new SelectList(_context.Produkties, "Id", "NazwaProduktu", szczegolyZamowienium.IdProduktu);
            ViewData["IdZamowienia"] = new SelectList(_context.Zamowienia, "IdZamowienia", "IdZamowienia", szczegolyZamowienium.IdZamowienia);
            return View(szczegolyZamowienium);
        }

        // POST: SZ/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdSzczegolyZamowienia,IdZamowienia,IdProduktu,IloscZamowionegoProduktu")] SzczegolyZamowienium szczegolyZamowienium)
        {
            if (id != szczegolyZamowienium.IdSzczegolyZamowienia)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(szczegolyZamowienium);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SzczegolyZamowieniumExists(szczegolyZamowienium.IdSzczegolyZamowienia))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdProduktu"] = new SelectList(_context.Produkties, "Id", "NazwaProduktu", szczegolyZamowienium.IdProduktu);
            ViewData["IdZamowienia"] = new SelectList(_context.Zamowienia, "IdZamowienia", "IdZamowienia", szczegolyZamowienium.IdZamowienia);
            return View(szczegolyZamowienium);
        }

        // GET: SZ/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var szczegolyZamowienium = await _context.SzczegolyZamowienia
                .Include(s => s.IdProduktuNavigation)
                .Include(s => s.IdZamowieniaNavigation)
                .FirstOrDefaultAsync(m => m.IdSzczegolyZamowienia == id);
            if (szczegolyZamowienium == null)
            {
                return NotFound();
            }

            return View(szczegolyZamowienium);
        }

        // POST: SZ/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var szczegolyZamowienium = await _context.SzczegolyZamowienia.FindAsync(id);
            _context.SzczegolyZamowienia.Remove(szczegolyZamowienium);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SzczegolyZamowieniumExists(int id)
        {
            return _context.SzczegolyZamowienia.Any(e => e.IdSzczegolyZamowienia == id);
        }
    }
}
