﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SklepDotNet.Models;

namespace SklepDotNet.Controllers
{
    public class StatusyZamowieniumsController : Controller
    {
        private readonly ShopDotNetContext _context;

        public StatusyZamowieniumsController(ShopDotNetContext context)
        {
            _context = context;
        }

        // GET: StatusyZamowieniums
        public async Task<IActionResult> Index()
        {
            return View(await _context.StatusyZamowienia.ToListAsync());
        }

        // GET: StatusyZamowieniums/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusyZamowienium = await _context.StatusyZamowienia
                .FirstOrDefaultAsync(m => m.IdStatusu == id);
            if (statusyZamowienium == null)
            {
                return NotFound();
            }

            return View(statusyZamowienium);
        }

        // GET: StatusyZamowieniums/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: StatusyZamowieniums/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdStatusu,NazwaStatusu")] StatusyZamowienium statusyZamowienium)
        {
            if (ModelState.IsValid)
            {
                _context.Add(statusyZamowienium);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(statusyZamowienium);
        }

        // GET: StatusyZamowieniums/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusyZamowienium = await _context.StatusyZamowienia.FindAsync(id);
            if (statusyZamowienium == null)
            {
                return NotFound();
            }
            return View(statusyZamowienium);
        }

        // POST: StatusyZamowieniums/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdStatusu,NazwaStatusu")] StatusyZamowienium statusyZamowienium)
        {
            if (id != statusyZamowienium.IdStatusu)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(statusyZamowienium);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatusyZamowieniumExists(statusyZamowienium.IdStatusu))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(statusyZamowienium);
        }

        // GET: StatusyZamowieniums/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var statusyZamowienium = await _context.StatusyZamowienia
                .FirstOrDefaultAsync(m => m.IdStatusu == id);
            if (statusyZamowienium == null)
            {
                return NotFound();
            }

            return View(statusyZamowienium);
        }

        // POST: StatusyZamowieniums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var statusyZamowienium = await _context.StatusyZamowienia.FindAsync(id);
            _context.StatusyZamowienia.Remove(statusyZamowienium);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StatusyZamowieniumExists(int id)
        {
            return _context.StatusyZamowienia.Any(e => e.IdStatusu == id);
        }
    }
}
