﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SklepDotNet.Models;
using SklepSklepDotNet;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SklepDotNet.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ShopDotNetContext _context;

        public HomeController(ILogger<HomeController> logger, ShopDotNetContext context)
        {
            _logger = logger;
            _context = context;
        }

        
        public async Task<IActionResult> Index()
        {
            
            var shopDotNetContext = _context.Produkties.Include(p => p.IdKategoriaNavigation);
            return View(await shopDotNetContext.ToListAsync());
        }
        [HttpPost]
        public async Task<IActionResult> Index(string query)
        {
            ViewBag.quer = query;
            
            var p = _context.Produkties.FromSqlRaw(sql:"SELECT * FROM Produkty WHERE NazwaProduktu LIKE '%"+ query+ "%'").ToListAsync();
            return View(await p);
        }

        [Route("Przelicz")]
        public IActionResult Przelicz(CurrencyType CurrTo)
        {
            var converter1 = new Converterr("bec007dd3b1548b27f7d");
            SessionHp.SetObjectAsJson(HttpContext.Session, "CurrTo", CurrTo);
           
            ViewBag.CurrTo = CurrTo;
            var fxCeny = SessionHp.GetObjectFromJson<List<decimal>>(HttpContext.Session, "fxCena");
            var fxCeny2 = new List<double>();
            foreach (var item in fxCeny)
            {
                fxCeny2.Add(converter1.Convert(Convert.ToDouble(item), CurrencyType.PLN, CurrTo));
            }
            ViewBag.PrzeliczonaCena = true;
            SessionHp.SetObjectAsJson(HttpContext.Session, "fxCeny2", fxCeny2);
            return RedirectToAction("Index");
        }

       

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Admin()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
