﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SklepDotNet.Models;

namespace SklepDotNet.Controllers
{
    public class ZamowieniumsController : Controller
    {
        private readonly ShopDotNetContext _context;

        public ZamowieniumsController(ShopDotNetContext context)
        {
            _context = context;
        }

        // GET: Zamowieniums
        public async Task<IActionResult> Index()
        {
            var shopDotNetContext = _context.Zamowienia.Include(z => z.IdKlientaNavigation).Include(z => z.IdStatusuZamowieniaNavigation);
            return View(await shopDotNetContext.ToListAsync());
        }

        // GET: Zamowieniums/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zamowienium = await _context.Zamowienia
                .Include(z => z.IdKlientaNavigation)
                .Include(z => z.IdStatusuZamowieniaNavigation)
                .FirstOrDefaultAsync(m => m.IdZamowienia == id);
            if (zamowienium == null)
            {
                return NotFound();
            }

            return View(zamowienium);
        }

        // GET: Zamowieniums/Create
        public IActionResult Create()
        {
            ViewData["IdKlienta"] = new SelectList(_context.Kliencis, "IdKlienta", "AdresKlienta");
            ViewData["IdStatusuZamowienia"] = new SelectList(_context.StatusyZamowienia, "IdStatusu", "NazwaStatusu");
            return View();
        }

        // POST: Zamowieniums/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("IdZamowienia,IdKlienta,WartoscZamowienia,DataZlozeniaZamowienia,IdStatusuZamowienia")] Zamowienium zamowienium)
        {
            if (ModelState.IsValid)
            {
                _context.Add(zamowienium);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKlienta"] = new SelectList(_context.Kliencis, "IdKlienta", "AdresKlienta", zamowienium.IdKlienta);
            ViewData["IdStatusuZamowienia"] = new SelectList(_context.StatusyZamowienia, "IdStatusu", "NazwaStatusu", zamowienium.IdStatusuZamowienia);
            return View(zamowienium);
        }

        // GET: Zamowieniums/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zamowienium = await _context.Zamowienia.FindAsync(id);
            if (zamowienium == null)
            {
                return NotFound();
            }
            ViewData["IdKlienta"] = new SelectList(_context.Kliencis, "IdKlienta", "AdresKlienta", zamowienium.IdKlienta);
            ViewData["IdStatusuZamowienia"] = new SelectList(_context.StatusyZamowienia, "IdStatusu", "NazwaStatusu", zamowienium.IdStatusuZamowienia);
            return View(zamowienium);
        }

        // POST: Zamowieniums/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IdZamowienia,IdKlienta,WartoscZamowienia,DataZlozeniaZamowienia,IdStatusuZamowienia")] Zamowienium zamowienium)
        {
            if (id != zamowienium.IdZamowienia)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(zamowienium);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ZamowieniumExists(zamowienium.IdZamowienia))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdKlienta"] = new SelectList(_context.Kliencis, "IdKlienta", "AdresKlienta", zamowienium.IdKlienta);
            ViewData["IdStatusuZamowienia"] = new SelectList(_context.StatusyZamowienia, "IdStatusu", "NazwaStatusu", zamowienium.IdStatusuZamowienia);
            return View(zamowienium);
        }

        // GET: Zamowieniums/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var zamowienium = await _context.Zamowienia
                .Include(z => z.IdKlientaNavigation)
                .Include(z => z.IdStatusuZamowieniaNavigation)
                .FirstOrDefaultAsync(m => m.IdZamowienia == id);
            if (zamowienium == null)
            {
                return NotFound();
            }

            return View(zamowienium);
        }

        // POST: Zamowieniums/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var zamowienium = await _context.Zamowienia.FindAsync(id);
            _context.Zamowienia.Remove(zamowienium);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ZamowieniumExists(int id)
        {
            return _context.Zamowienia.Any(e => e.IdZamowienia == id);
        }
    }
}
