﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SklepDotNet.Models;
using Microsoft.EntityFrameworkCore;

namespace SklepDotNet.Controllers
{
    [Route("cart")]
    public class CartController : Controller
    {
        private readonly ShopDotNetContext _context;

        public CartController( ShopDotNetContext context)
        {
            _context = context;
        }

        [Route("Koszyk")]
        public IActionResult Koszyk()
        {
         
            return View();
        }

        [Route("Koszyk/{id?}")]
        public IActionResult Koszyk(int id)
        {
            
            var model = new Produkty();
            if (SessionHp.GetObjectFromJson<List<SzczegolyZamowienium>>(HttpContext.Session, "koszyk")==null)
            {
                List<SzczegolyZamowienium> koszyk = new List<SzczegolyZamowienium>();

               
                var x = _context.Produkties.Where(p => p.Id==id).ToList();
                //backup
                //koszyk.Add(new SzczegolyZamowienium { IdProduktuNavigation = model = x[0], IloscZamowionegoProduktu = 1 });
                koszyk.Add(new SzczegolyZamowienium {IdProduktu=x[0].Id ,IdProduktuNavigation = model = x[0], IloscZamowionegoProduktu = 1 });
                SessionHp.SetObjectAsJson(HttpContext.Session, "koszyk", koszyk);
                ViewBag.koszyk = koszyk;
            }
            else
            {
                List<SzczegolyZamowienium> koszyk = SessionHp.GetObjectFromJson<List<SzczegolyZamowienium>>(HttpContext.Session, "koszyk");
                var x = _context.Produkties.Where(p => p.Id == id).ToList();
                //backup
                //koszyk.Add(new SzczegolyZamowienium { IdProduktuNavigation = model = x[0], IloscZamowionegoProduktu = 1 });
                koszyk.Add(new SzczegolyZamowienium {IdProduktu=x[0].Id ,IdProduktuNavigation = model = x[0],IloscZamowionegoProduktu =1 });
                SessionHp.SetObjectAsJson(HttpContext.Session, "koszyk", koszyk);
                ViewBag.koszyk = koszyk;
            }
             
            return View();
        }

        [Route("Remove/{id?}")]
        public IActionResult Remove(int id)
        {
            List<SzczegolyZamowienium> koszyk = SessionHp.GetObjectFromJson<List<SzczegolyZamowienium>>(HttpContext.Session, "koszyk");
            int idx = CzyJest(id);
            koszyk.RemoveAt(idx);
            SessionHp.SetObjectAsJson(HttpContext.Session, "koszyk", koszyk);

            return RedirectToAction("Koszyk");
        }

        private int CzyJest(int id)
        {
            List<SzczegolyZamowienium> koszyk = SessionHp.GetObjectFromJson<List<SzczegolyZamowienium>>(HttpContext.Session, "koszyk");
            for (int i = 0; i < koszyk.Count; i++)
            {
                if (koszyk[i].IdProduktuNavigation.Id.Equals(id))
                {
                    return i;
                }
            }
            return -1;
        }
    }

   
}
