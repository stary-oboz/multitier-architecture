﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SklepDotNet.Models;

namespace SklepDotNet.Controllers
{
    public class ZlozZamController : Controller
    {
        private readonly ShopDotNetContext _context;
        DateTime teraz = DateTime.Now;

        public ZlozZamController(ShopDotNetContext context)
        {
            _context = context;
        }

        // GET: ZlozZam
        public async Task<IActionResult> Index()
        {
            var shopDotNetContext = _context.Zamowienia.Include(z => z.IdKlientaNavigation).Include(z => z.IdStatusuZamowieniaNavigation);
            return View(await shopDotNetContext.ToListAsync());
        }

       

        // GET: ZlozZam/Create
        public IActionResult Create()
        {
           
            
            return View();
        }

        // POST: ZlozZam/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            string EmailK, string AdresK,
            [Bind("EmailKlienta,AdresKlienta")] Klienci klient,
            [Bind("IdZamowienia,IdKlienta,WartoscZamowienia,DataZlozeniaZamowienia,IdStatusuZamowienia")] Zamowienium zamowienium,
            [Bind("IdSzczegolyZamowienia,IdZamowienia,IdProduktu,IloscZamowionegoProduktu")] SzczegolyZamowienium szczegolyZamowienia

            )
        {
            if (ModelState.IsValid)
            {
                //dodawanie klientów 
                SessionHp.SetObjectAsJson(HttpContext.Session, "email", EmailK);
                klient.EmailKlienta = EmailK;
                klient.AdresKlienta = AdresK;
                _context.Add(klient);
                await _context.SaveChangesAsync();
                //dodawanie zamówień
                var kId = _context.Kliencis.FromSqlRaw(sql: "SELECT * From Klienci Where EmailKlienta like '"+ EmailK +"'").ToList();
                zamowienium.IdKlienta = kId[0].IdKlienta;
                _context.Add(zamowienium);
                await _context.SaveChangesAsync();
                //dodawanie szczegółów zamówień
                var sz = SessionHp.GetObjectFromJson<List<SzczegolyZamowienium>>(HttpContext.Session, "koszyk");
                var zam = _context.Zamowienia.FromSqlRaw(sql: "Select * From Zamowienia Where IdKlienta Like '" + kId[0].IdKlienta + "' Order by IdKlienta desc").ToList();
                SessionHp.SetObjectAsJson(HttpContext.Session, "NrZam", zam[0].IdZamowienia);
                var zam2 = SessionHp.GetObjectFromJson<int>(HttpContext.Session, "NrZam");

                foreach (var item in sz)
                {

                    szczegolyZamowienia.IdZamowienia = zam2;
                    szczegolyZamowienia.IdProduktu = item.IdProduktuNavigation.Id;
                    szczegolyZamowienia.IloscZamowionegoProduktu = item.IloscZamowionegoProduktu;
                    var prd = _context.Produkties.Where(p => p.Id == item.IdProduktuNavigation.Id).ToList();
                    prd[0].IloscWMagazynie -= item.IloscZamowionegoProduktu;
                    _context.Update(prd[0]);
                    _context.Add(szczegolyZamowienia);
                }
               
                
                
                await _context.SaveChangesAsync();
                return RedirectToAction("Dodano");
            }
  
            
            return View();
        }

        [Route("Dodano")]
        public IActionResult Dodano()
        {
            return View();
        }


        private bool ZamowieniumExists(int id)
        {
            return _context.Zamowienia.Any(e => e.IdZamowienia == id);
        }
    }
}
